function n(){}

module.exports = {
	assert : n,
	count : n,
	debug : n,
	dir : n,
	dirxml : n,
	error : n,
	group : n,
	groupCollapsed : n,
	groupEnd : n,
	info : n,
	log : n,
	profile : n,
	profileEnd : n,
	table : n,
	time : n,
	timeEnd : n,
	trace : n,
	warn : n
};